# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="~/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
#ZSH_THEME="juanghurtado"
ZSH_THEME="michelebologna"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
ZSH_THEME_RANDOM_CANDIDATES=( "fino-time" "agnoster" "blinks" "clean" "crcandy" "dallas" "dogenpunk" "daveverwer" "duellj" "fletcherm" "frisk" "gozilla" "jaischeema" "jispwoso" "jonathan" "juanghurtado" "linuxonly" "michelebologna" "murilasso" "nebirhos" "rkj-repos" "simonoff" "smt" "Soliah" "sporty_256" "wuffers" "xiong-chiamiov-plus" "zhann")

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
HIST_STAMPS="yyyy-mm-dd-hh"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(aliases alias-finder ansible ant asdf battery bedtools branch bundler colored-man-pages colorize command-not-found common-aliases compleat copybuffer copydir copyfile cp dircycle dirhistory docker docker-compose doctl dotenv droplr emoji encode64 extract fabric fancy-ctrl-z fasd fastfile fd firewalld flutter fnm forklift gas gatsby gb geeknote git git-auto-fetch git-escape-magic git-extras gitfast git-flow git-flow-avh github gitignore git-lfs git-prompt gradle gnu-utils golang history history-substring-search invoke ipfs  jfrog jhbuild jsontools kitchen kops kubectl kubectx kube-ps1 last-working-dir man microk8s minikube mongocli mosh mvn nanoc ng nmap node nomad npm npx nvm oc pass  pip pipenv pj pm2 pod postgres pow powify profiles pyenv pylint python redis-cli repo ros rsync safe-paste salt sbt scala scd sdk sfdx sfffe ssh-agent sudo supervisor svn systemadmin systemd taskwarrior terminitor terraform themes tig tmux tmux-cssh tmuxinator urltools vault vim-interaction virtualenv virtualenvwrapper vscode vundle wd web-search z zsh-interactive-cd zsh-navigation-tools git)

source $HOME/.oh-my-zsh/oh-my-zsh.sh
export TERM=xterm-256color

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8
#source ~/.dotfiles/.profile

#!/bin/bash
set -x
set -v
set -e
set -u
set -f
# Set variables
#export NODE_IP4="$(hostname -i)"
#export NODE_HOSTNAME="$(hostname)"
#export NODE_ROLE=control-plane
#echo "$NODE_IP4 $NODE_HOSTNAME" >> /etc/hosts

yum install golang net-tools wget telnet yum-utils device-mapper-persistent-data -y
yum install lvm2 nfs-utils kubernetes-cni cri-tools docker-logrotate  -y
yum install podman buildah skopeo containerd flannel calico -y
yum install docker-ce docker-ce-cli docker-compose docker-ce-rootless-extras docker-scan-plugin -y --skip-broken
yum install python3 python3-pip netdata nfs-utils -y
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR

systemctl enable docker

mkdir -p /etc/docker

# There is a big issue right now with cgroups. Docker wants to use cgroups to manage 
# them and the Kubernetes developers want everyone running on systemd. So, here we 
# Change the Docker daemon over to use systemd (it's always cgroups by default).
cat > /etc/docker/daemon.json <<EOF
{
  "group":"docker"
}
EOF
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR

systemctl start docker
systemctl daemon-reload
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR

# hostname is important to kubernetes operation. Set it.
export NODE_HOSTNAME="$(hostname)"

# Swap.

# Kubernetes can't run with swap enabled
# Temporary fix
eval `swapoff -a`
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
# Permanent fix
/bin/sed -i 's/^.*swap.*$/#&/' /etc/fstab
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
echo $(cat /etc/fstab)
# SELinux MUST be disabled for kubernetes to operate
# Temporary fix
export SELINUX_STATUS=$(getenforce)
if [[ $SELINUX_STATUS != "Disabled" ]]; then
    setenforce 0
fi
echo "SELinux is currently: $SELINUX_STATUS"
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
sed -i  's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
# Rancher and Kubernetes  Pre-install script for RHEL7
echo "Basically all nodes in your cluster must have all ports ready and waiting for a connection"
echo "see the output of $ netstat -plnt after kube is installed to see."
# Prepare master node
yum update -y 

# Disable swap (mandatory on all nodes)
eval `ssh-agent -s`
# Now, `sudo edit /etc/fstab` and comment out any lines that say "swap"
echo "DO THIS>> sudo vim /etc/fstab<< and comment out any swap entries"

# Ensure the right ports are kept open
# firewall-cmd --permanent --zone=docker --add-port=6443/tcp
# firewall-cmd --permanent --zone=docker --add-port=2379-2380/tcp
# firewall-cmd --permanent --zone=docker --add-port=10250/tcp
# firewall-cmd --permanent --zone=docker --add-port=10251/tcp
# firewall-cmd --permanent --zone=docker --add-port=10252/tcp
# firewall-cmd --permanent --zone=docker --add-port=10255/tcp
# firewall-cmd --permanent --zone=docker --add-port=8285/tcp
# firewall-cmd --permanent --zone=docker --add-port=80/tcp
# firewall-cmd --permanent --zone=docker --add-port=443-2380/tcp
# firewall-cmd --permanent --zone=docker --add-port=8443/tcp
# firewall-cmd --permanent --zone=docker --add-port=8080/tcp
# firewall-cmd --permanent --zone=docker --add-port=5555/tcp
# firewall-cmd --permanent --zone=docker --add-port=22/tcp
# firewall-cmd --permanent --zone=docker --add-port=30000-32767/tcp
# firewall-cmd --permanent --zone=docker --add-port=8285/tcp
# firewall-cmd --permanent --zone=docker --add-port=8472/tcp
# firewall-cmd --permanent --zone=docker --add-port=179/tcp
# firewall-cmd --permanent --zone=docker --add-port=4789/tcp
# firewall-cmd --permanent --zone=docker --add-port=8443/tcp
# firewall-cmd --permanent --zone=docker --add-port=9099/tcp
# firewall-cmd --permanent --zone=docker --add-port=9100/tcp
# firewall-cmd --permanent --zone=docker --add-port=9443/tcp
# firewall-cmd --permanent --zone=docker --add-port=6783-6784/tcp
# firewall-cmd --permanent --zone=docker --add-port=10254/tcp
# firewall-cmd --reload
 modprobe br_netfilter
# This is in the docs but it won't even run as sudo so I found a workaround. 
#sudo  echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables
sysctl -w net.bridge.bridge-nf-call-iptables=1
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR

systemctl disable firewalld

# Enable IP Forwarding
echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables

cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF


# Restarting services
systemctl daemon-reload

# Rancher
# Get the Rancher group
export RANCHER_GROUP=$(getent group rancher | cut --delimiter=: -f1)
# Create group "rancher"
if [[ $RANCHER_GROUP != "rancher" ]]; then
  groupadd rancher
fi
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
export DOCKER_GROUP=$(getent group docker | cut --delimiter=: -f1) 
# Creaate group "docker" if not exists, and chown it so the group owner is docker
if [[ $DOCKER_GROUP != "docker" ]]; then
    groupadd docker # It should already exist
fi


export RANCHER_ID=`id -u rancher`
echo   "RANCHER_ID is $RANCHER_ID"
function homedir() {
    echo "This function takes one argument: the full path of the directory."
    echo "The directory to be created is: $1"
    if [[ ! -d $1 ]]; then
        # Call function to make the directory and cd to it (func declared below
        mkcd $1
        export DIRECTORY=$1
        if [[ -d $DIRECTORY ]]; then
            echo "The directory $1 has been created successfully."
        else
            echo "Creating directory $DIRECTORY failed. It may already exist"
        fi
    fi
}
export HOME="/net/scfdevlab/home/"
export RANCHER_HOME="/net/scfdevlab/home/rancher"

function get_keys(){
    # 5  Arguments: 
    # 1. The user
    # 2. The server to be accessed
    # 3. The .ssh directory to pull the keys
    # 4. The private  key name (id_rsa)
    # 5. The public key name (id_rsa.pub)
    if [[ $4 == 'id_rsa' ]]; then
        if [[ -f $4  ]]; then
            mv $4 $4.bak
        fi
    fi
    if [[ $5 == 'id_rsa.pub' ]]; then
        if [[ -f $5 ]]; then
            mv $5 $5.bak
        fi
    fi
    
    #scp $1\@$2:$3\/$4* $RANCHER_HOME/.ssh/.
    trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
    if [[ -f $RANCHER_HOME/.ssh/id_rsa.pub ]]; then
        cd $RANCHER_HOME/.ssh/
        # Add the downloaded public key to authorized keys (use ">>" not ">"
        cat id_rsa.pub >> authorized_keys
        echo "The public key has been added to authorized_keys"
    fi
    echo "The files have been transferred to $RANCHER_HOME/.ssh/."
    ls -al $RANCHER_HOME/.ssh/
}

if [[ ! -d "/net/scfdevlab/home/rancher" ]]; then
  mkdir -p /net/scfdevlab/home/rancher
  chmod -R 755 /net/scfdev/home/rancher
  chown -R rancher:rancher /net/scfdev/home/rancher
fi
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR

USER=rancher
SERVER=vmdev-isam.socdev.intelsat.com
SSH_DIRECTORY=$RANCHER_HOME.ssh/
PRIVATE_KEY=id_rsa
PUBLIC_KEY=id_rsa.pub
get_keys $USER $SERVER $SSH_DIRECTORY $PRIVATE_KEY $PUBLIC_KEY
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
echo "Dump of settings: $-"

function mkuser(){
    if [[ -z  $1 ]]; then
        useradd rancher -d $RANCHER_HOME -g docker -G rancher 
    else
        echo "User $1  already exists."
        cd $RANCHER_HOME
    fi
}
mkuser rancher
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR

if [[ ! -d "$RANCHER_HOME/.ssh/" ]]; then
    mkdir -p $RANCHER_HOME/.ssh/
fi

function mkcd(){
  echo "Takes one argument - the name of the directory and cds to it."
  if [[ ! -d $1 ]]; then
    mkdir -p -- "$1" && cd -P -- "$1"
  else
    # Just cd to it.
    cd $1/
  fi
}
echo "Directory: $HOME/.ssh"

function check_name(){
  # Get group
  
# Get username
export  NAME=`id -u $1`
#if not exists
  if [[ -z $NAME ]]; then
    useradd $1 -d $GROUP -g docker  -G $1
  fi

}
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
if [[ ! -f $RANCHER_HOME/.ssh/rancher ]]; then
    ssh-keygen -b 2048 -t rsa -f $HOME/.ssh/rancher -q -N ""
    cd $HOME/.ssh/
    chown -R rancher:rancher $HOME/.ssh/.
    ssh-add rancher
    cat rancher.pub >> authorized_keys
fi
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
eval `ssh-agent -s`
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
#ssh-add $HOME/.ssh/*
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
export HELM_EXPERIMENTAL_OCI=1

chmod 750 -R /var/run/docker.sock
chown root:docker /var/run/docker.sock 

#ensure groups "docker", "dockerrootless", containerd if it exists, and the rancher group
#usermod -aG rancher docker 
usermod -aG docker rancher
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR

#sudo vim /etc/group
#    then add the followiing users to the docker group:
#        docker:x:976:rancher,matusa,podman 
 
systemctl daemon-reload
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
systemctl restart docker
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
systemctl enable docker
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
cd $RANCHER_HOME
if [[ ! -f /usr/local/bin/rke ]]; then
    curl https://github.com/rancher/rke/releases/download/v1.3.1/rke_linux-amd64
    trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
    trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
    trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
fi
if [[ ! -f /usr/local/bin/rke ]]; then
    cp rke /usr/local/bin/
fi
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
chmod +x rke
chown -R rancher:rancher $RANCHER_HOME
#mv rke /usr/local/bin/rke 
#just to be sure the rancher user can run it, we have to add it to the path.
export PATH=/usr/local/bin:$PATH
yum update -y 
systemctl enable docker
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
systemctl restart docker
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR

# (if using a full k8s setup, we will need this repository.)
# Install kuberentes packages
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR

# Install kubectl and kubeadm. These are very useful tools. 
yum update -y
yum install kubectl kubeadm -y
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
# Manually add some of the big boy kubernetes tools
# yum install kubeadm kubectl kubernetes-cni -y
#NOTE: I'm suspicious that kubernetes-cni is conflicting with calico / flannel, the kube networking addons that you have to use (you need at least one, and an ingress controller) 


#Copy rke exe from rancher home
if [[ ! -f "/usr/local/bin/rke" ]]; then
    if [[ ! -d "$RANCHER_HOME/rancher/src/rke/home/rancher/rke/bin/rke" ]]; then
      cp -rf $RANCHER_HOME/rancher/src/rke/home/rancher/rke/bin/rke /usr/local/bin/rke
    fi
fi
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR

# TODO: Download and install helm.

systemctl daemon-reload
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
systemctl restart docker
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR
systemctl enable docker
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR

echo "THE SCRIPT HAS COMPLETED!"
echo "Let's run rke up!"
ssh-add $RANCHER_HOME/.ssh/id_rsa
cat $RANCHER_HOME/.ssh/id_rsa.pub >> $RANCHER_HOME/.ssh/authorized_keys 
/usr/local/bin/rke --debug up
trap 'echo "$BASH_COMMAND" failed with error code $?' ERR

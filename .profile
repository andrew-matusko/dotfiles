alias z="vim $HOME/.zshrc"
alias v="vim $HOME/.vimrc"
alias s="systemctl "
alias sr="systemctl restart "
alias ss="systemctl status "
alias st="systemctl start "
alias gaa="git add --all"
alias gcm="git commit -m "
alias n="netstat -plnt"
alias hg="history | grep "
alias ll="ls -al"
alias ls="ls -al"
alias lh="ls -halt"

# Performs a du -sh * in the CWD and sorts it. 
# Only shows files a MB or above.
function dirsize(){
    # if first param is there, then check that directory.
    if [[ $1 != null && -d $1 ]]; then
      sudo du -sh $1/* | grep -Ev "[GM]" | sort -r -n | head -n 20;
    else
      echo "No directory specified. Here are the results of root.";
      sudo du -sh * | grep -Ev "[GM]" | sort -r -n | head -n 20;
    fi
}

function mkcd ()
{
    mkdir -p -- "$1" && cd -P -- "$1"
}
function pg ()
{
    #greps running processes with a single argument
    ps -ef | {
        read -r;
        printf '%s\n' "$REPLY";
        grep --color=auto "$@"
    }
}
#Shows file size
alias lt='ls --human-readable --size -1 -S --classify'
alias mnt="mount | awk -F' ' '{ printf \"%s\t%s\n\",\$1,\$3; }' | column -t | egrep ^/dev/ | sort"
alias count='find . -type f | wc -l'
alias gittop='cd `git rev-parse --show-toplevel`'
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"
alias myip="curl http://ipecho.net/plain; echo"
#alias common="history | awk '{CMD[$2]++;count++;}END { for (a in CMD)print CMD[a] " " CMD[a]/count*100 "% " a;}' | grep -v "./" | column -c3 -s " " -t | sort -nr | nl |  head -n10"
function tarball() {
        echo "Takes 2 arguments. The archive to create, and the file or dir to archive."
        echo "tar -zcvf archive.tar.gz foo bar  # Create archive.tar.gz from files foo and bar."
    read -r -p '?Do you want to continue? ' choice

    case "$choice" in
      n|N) break;;
      y|Y) tar -zcvf $1 $2;;
      *) echo 'Response not valid';;
    esac
}
function untar() {
	#echo "Call this function with the filename.tar.gz as the first parameter."
	#read -r -p  '?Do you want to continue? ' choice
	#case "$choice" in
	# n|N) break;;
	#  y|Y) tar -xvzf $1;;
  #        *) echo 'Response not valid';;
	#esac

  tar -xvzf $1 .
}
numfiles() {
    N="$(ls $CWD | wc -l)";
    echo "$N files in $PWD";
}
function dirsize(){
    echo "Takes one argument: the path to the directory to size up."
    du -hs $1  | sort -n -r | head -n 5
}
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
export HOME_BASE=/net/scfdevlab/home/
export RANCHER_HOME=$HOME_BASE/rancher
export HISTTIMEFORMAT='%F %T '
function trap_errors(){
  # Add more debug to each command. 
  trap "echo $( echo $( for i in *; do echo $i; done ) )" DEBUG
  return $?
}
#export PS4=debug_trap
#declare -r PROMPT_COMMAND=$(debug_trap)
export TERM=xterm-256color
#export PS4=trap_errors
#declare -r PROMPT_COMMAND=$(trap_errors)
function is-dir(){
  if [[ -d $1 ]]; then 
    return true
  else
    return false
  fi
}
function isDirOrFile()
{
	if [[ $1 == 'f' || $1 == 'file' ]]; then
		return 'file'
	elif [[ $1 == 'd' || $1 == 'dir' || $1 == 'directory' || $1 == 'folder' ]]; then
		return 'directory'
	else
		return false
	fi
}
export PATH=$PATH:/bin:/usr/bin:/opt:/usr/local/bin:/usr/local/go/bin:$HOME/.local/bin:$HOME/bin:$HOME/.brew/bin:$HOME/.brew/sbin:/Users/andrew.matusko/Library/Python/3.8/bin:$HOME/homebrew/bin:$PATH
export KUBECONFIG=/docker/rke/kube_config_cluster.yml
#alias  vim=/usr/local/bin/nvim
export ZSH=~/.oh-my-zsh
function dump_variables() {
    for var in "$@"; do
    echo "$var=${!var}"
    done
}
# STRING="Hello World"
# ARRAY=("ab" "cd")
# dump_variables STRING ARRAY ARRAY[@]

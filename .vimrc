set ruler
" TODO: find out what this feature does. 
filetype indent plugin on
" TODO: find out what this feature does, and why I do or do not need it. 
set tabstop=2
" TODO: find out what this feature does. 
set expandtab
" TODO: find out EXACTLY what this does. Is this the same as "tabstop=2"?
set shiftwidth=2
" Giving autoindent a try once again since paste is turned on now by default
" TODO: test whether "set shiftwidth=2" should remain or be commented. 
set ai
" Enables you to paste from your machine into vim without crazy formatting messing it up
set paste
" Highlight every instance of your search terms
set hlsearch
" Search one item at a time using "n" to go to the next instance or "N" to go to the previous
set incsearch
" Widen the visible textwidth a bit because the standard 80 is a bit small.
set textwidth=100
" Incrementally write changes to a file that's being edited.
set autowrite
" TODO: Find out if we can get more debug output here or with a plugin
set highlight=l:ErrorMsg
" Ignore each  character's case, case-insensitive
set ignorecase
" TODO: Find out what this does, exactly.
set smartcase
" TODO: Find out what this does, exactly
set scrolloff=2
" This feature allows you to hit tab in the gutter when searching for something like :colorscheme and returns all the possible options
set wildmode=longest
" For compatibility with both Vi, and old versions of Vim that lack newer features.
set nocompatible
" Never change this because swapfiles are fucking awful and always give you an error when you try to reopen.
set noswapfile
" enable syntax highlighting
syntax enable
" show line numbers
set number
" set tabs to have 4 spaces
set ts=2
" indent when moving to the next line while writing code
set autoindent
" expand tabs into spaces
set expandtab
" when using the >> or << commands, shift lines by 4 spaces
set shiftwidth=2
" show a visual line under the cursor's current line
set cursorline
" show the matching part of the pair for [] {} and ()
set showmatch
" enable all Python syntax highlighting features
let python_highlight_all = 1
" Set Modeline
set modeline

" display settings
" enable for dark terminals
set nowrap              " dont wrap lines 
set scrolloff=2         " 2 lines above/below cursor whe
set showmatch
" show matching bracket (briefly jump)
set showmode            " show mode in status bar (insert/replace/..
set showcmd             " show typed command in status b
set ruler               " show cursor position in status ba
set title               " show file in titleba
set wildmenu            " completion with me
set wildignore=*.o,*.obj,*.bak,*.exe,*.swp
set laststatus=5        " use 5 lines for the status bar set matchtime=2      
" show matching bracket for 0.2 second
set matchpairs+=<:>
set confirm
"Enable iTerm to use 256 colors in vim
set t_Co=256
" I prefer koheler, murphy but setting to slate so I don't mistake dev for prod with both windows open
syntax enable
set background=dark
map bs "#!/bin/bash/<ESC>o
" required
filetype plugin indent on 
let g:SimpylFold_docstring_preview = 1
" Enable folding
set foldmethod=indent
set foldlevel=99

" Enable Folding with the Spacebar
map <space> za

" Navigate Splits
map <C-d> <C-W><C-J>
map <C-u> <C-W><C-K>
map <C-r> <C-W><C-L>
map <C-l> <C-W><C-H>

"autocomplete
let g:ycm_autoclose_preview_window_after_completion=1



colorscheme brighton
" Settings splits
set splitbelow
set splitright
" Enable folding
set foldmethod=indent
set foldlevel=99
" Enable folding with the spacebar
nnoremap <space> za
let g:SimpylFold_docstring_preview=1

""""
" Python3 Specific Settings
""""
" For all python files, set environment to follow PEP8 specifications
au BufNewFile,BufRead *.py
    \ set tabstop=4
    \ set softtabstop=4
    \ set shiftwidth=4
    \ set textwidth=79
    \ set expandtab
    \ set autoindent
    \ set fileformat=unix

" Ensure that html, javaScript, and CSS files still get the proper two spaces per tab
au BufNewFile,BufRead *.js, *.html, *.css
    \ set tabstop=2
    \ set softtabstop=2
    \ set shiftwidth=2



"""
" Navigate Splits
" Use the same vim keys that you should be using to move around: k(up)
"""
" GO TO  UPPER SPLIT OR PANE with CTRL+k 
map <C-k> <C-W>k
" GO TO LOWER SPLIT OR PANE WITH CTRL+j
map <C-j> <C-W>j
" GO TO LEFT SPLIT OR PANE WITH CTRL+h
map <C-h> <C-W>h
" Use CTRL+l (Lowercase "L") to move to the right pane.
map <C-l> <C-W>l
" nonoremap <C-L> <C-M><C-L>
nnoremap bs i"!/bin/bash/<ESC>
" Plugins
set rtp+=~/.vim/bundle/Vundle.vim
call plug#begin('~/.vim/bundle/')
let g:NERDTreeGitStatusIndicatorMapCustom = {
                \ 'Modified'  :'✹',
                \ 'Staged'    :'✚',
                \ 'Untracked' :'✭',
                \ 'Renamed'   :'➜',
                \ 'Unmerged'  :'═',
                \ 'Deleted'   :'✖',
                \ 'Dirty'     :'✗',
                \ 'Ignored'   :'☒',
                \ 'Clean'     :'✔︎',
                \ 'Unknown'   :'?',
                \ }

let g:NERDTreeGitStatusUseNerdFonts = 1 " you should install nerdfonts by yourself. default: 0
let g:NERDTreeGitStatusShowIgnored = 1 " a heavy feature may cost much more time. default: 0
let g:NERDTreeGitStatusUntrackedFilesMode = 'all' " a heavy feature too. default: normal
let g:NERDTreeGitStatusShowClean = 1 " default: 0
let g:NERDTreeGitStatusConcealBrackets = 1 " default: 0
au FileType c,cpp,objc,objcpp call rainbow"load()
let g:rainbow_active = 1
let g:rainbow_active = 1
let g:rainbow_load_separately = [
    \ [ '*' , [['(', ')'], ['\[', '\]'], ['{', '}']] ],
    \ [ '*.tex' , [['(', ')'], ['\[', '\]']] ],
    \ [ '*.cpp' , [['(', ')'], ['\[', '\]'], ['{', '}']] ],
    \ [ '*.{html,htm}' , [['(', ')'], ['\[', '\]'], ['{', '}'], ['<\a[^>]*>', '</[^>]*>']] ],
    \ ]

let g:rainbow_guifgs = ['RoyalBlue3', 'DarkOrange3', 'DarkOrchid3', 'FireBrick']
let g:rainbow_ctermfgs = ['lightblue', 'lightgreen', 'yellow', 'red', 'magenta']
set foldtext=gitgutter"fold"foldtext()
function! GitStatus()
  let [a,m,r] = GitGutterGetHunkSummary()
  return printf('+%d ~%d -%d', a, m, r)
endfunction
nmap <F8> :TagbarToggle<CR> "hit the F8 key to enable
set statusline+=%{GitStatus()}
" Python-specific plugins
Plug 'vim-scripts/indentpython.vim', {'for': ['py', 'pyc']}
Plug 'python-mode/python-mode', {'for': ['py', 'pyc']}
Plug 'vim-scripts/python.vim', {'for': ['py', 'pyc']}
Plug 'vim-scripts/python.vim--Vasiliev', {'for': ['py', 'pyc']}
Plug 'vim-syntastic/syntastic'

" Syntastic Settings and Setup
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
" set statusline+=%*

let g:syntastic_enable_signs=1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 1

Plug 'faith/vim-go', { 'for': ['go'] }

Plug 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}

Plug 'vim-scripts/Pydiction', {'for': ['py','pyc']}
" Python plugin to ensure PEP8 conventions are followed.
Plug 'nvie/vim-flake8'
" Turn on Python Syntax Highlighting
let python_highlight_all=1
syntax on

"python with virtualenv support
py << EOF
import os
import sys
if 'VIRTUAL_ENV' in os.environ:
  project_base_dir = os.environ['VIRTUAL_ENV']
  activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
  execfile(activate_this, dict(__file__=activate_this))
EOF
" Python: flag any 'bad' whitespace
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/
" End Python plugins
" Offers Improved Code-Folding
Plug 'tmhedberg/SimpylFold'

au BufNewFile,BufRead *.py
    \ set tabstop=4
    \ set softtabstop=4
    \ set shiftwidth=4
    \ set textwidth=79
    \ set expandtab
    \ set autoindent
    \ set fileformat=unix

"""
" JavaScript and Node.js plugins (includes npm, nvm, npx)
"""
" Not sure if I should be associating this with 'js', 'node', or what. TODO: watch.
Plug 'moll/vim-node', {'for': ['js']}
" Want to customize settings for files inside a Node projects?
" Use the Node autocommand:
autocmd User Node if &filetype == "javascript" | setlocal expandtab | endif
" Want <C-w>f to open the file under the cursor in a new vertical split?
" <C-w>f by default opens it in a horizontal split. To have it open vertically, drop this in your vimrc:
autocmd User Node
  \ if &filetype == "javascript" |
  \   nmap <buffer> <C-w>f <Plug>NodeVSplitGotoFile |
  \   nmap <buffer> <C-w><C-f> <Plug>NodeVSplitGotoFile |
  \ endif

au BufNewFile,BufRead *.js, *.html, *.css
    \ set tabstop=2
    \ set softtabstop=2
    \ set shiftwidth=2

""" Run the Current Python File using F4
map <f4> :w\|!python %

" Python Mode Extension
Plug 'python-mode/python-mode', { 'for': [ 'py', 'pyc', 'python3', 'python' ]}

"""
" YAML
"""
Plug 'stephpy/vim-yaml', { 'for': ['yml', 'yanl'] }
"""
" JavaScript Plugins and Customizations
"""
Plug 'pangloss/vim-javascript', { 'for': ['js', 'json']}
"""
" Typescript Plugins and Customizations
"""
Plug 'leafgarland/typescript-vim', {'for': ['ts']}
"""
" C and C++ Plugins and Customizations
"""
Plug 'rhysd/vim-clang-format', { 'for': ['c','cpp','so'] }
"""
" Bash Plugins and Customizations
"""
" Powerful shell implemented by vim
Plug 'Shougo/vimshell.vim'  
Plug 'neoclide/coc.nvim'
" HTML, CSS, Markdown
"""
" HTML
Plug 'othree/html5.vim', { 'for': ['html','htm','shtml']}
"html
Plug 'isnowfy/python-vim-instant-markdown', { 'for': ['py', 'pyc'] }
Plug 'jtratner/vim-flavored-markdown', { 'for': ['md'] }
Plug 'suan/vim-instant-markdown'
Plug 'nelstrom/vim-markdown-preview'

"""
" YAML - ADO and GitLab
"""
Plug 'chase/vim-ansible-yaml', {'for': ['yaml', 'yml']}
" VIM YAML for Helm
Plug 'towolf/vim-helm', {'for': ['helm', 'yaml', 'yml']}

"""
" JSON Plugins and Customizations
"""
Plug 'elzr/vim-json', {'for': ['json']}
" Make sure these JSON Plugins don't Conflict.
Plug 'vim-scripts/vim-json-bundle', {'for': ['json']}
""" General vim behavior
""" Custom Colorschemes 
" Plug 'jnurmine/Zenburn'
" Plug 'altercation/vim-colors-solarized'
Plug 'flazz/vim-colorschemes'
"if has('gui_running')
"  set background=dark
"  colorscheme solarized
"else
"  colorscheme zenburn
"endif
"" Hit F5 to toggle from dark to light colorscheme
"call togglebg#map("<F5>")
" YouCompleteMe
Plug 'Valloric/YouCompleteMe'
let  g:ycm_autoclose_preview_window_after_completion=1
map  <leader>g  :ycm GoToDefinitionElseDeclaration<CR>
let g:SimpylFold_docstring_preview = 1
"------------Start Python PEP 8 stuff----------------
" Number of spaces that a pre-existing tab is equal to.
au BufRead,BufNewFile *py,*pyw,*.c,*.h set tabstop=4
"spaces for indents
au BufRead,BufNewFile *.py,*pyw set shiftwidth=4
au BufRead,BufNewFile *.py,*.pyw set expandtab
au BufRead,BufNewFile *.py set softtabstop=4

" Use the below highlight group when displaying bad whitespace is desired.
highlight BadWhitespace ctermbg=red guibg=red
" Display tabs at the beginning of a line in Python mode as bad.
au BufRead,BufNewFile *.py,*.pyw match BadWhitespace /^\t\+/
" Make trailing whitespace be flagged as bad.
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/
" Wrap text after a certain number of characters
au BufRead,BufNewFile *.py,*.pyw, set textwidth=100
" Use UNIX (\n) line endings.
au BufNewFile *.py,*.pyw,*.c,*.h,*.yaml,*.yml,*.json, *.js, *,ts, *.go, *.txt, *.conf, *.cnf, *.sh, $HOME/\.*, *.sql, *.csv, *.toml, *.vim, *.xml, *.html, *.css, *.php, *.cpp, *.java, *.class  set fileformat=unix
" Set the default file encoding to UTF-8:
set encoding=utf-8
" For full syntax highlighting:
let python_highlight_all=1
syntax on

" Keep indentation level from previous line:
autocmd FileType python set autoindent

" make backspaces more powerfull
set backspace=indent,eol,start

"Folding based on indentation:
autocmd FileType python set foldmethod=indent
"use space to open folds
nnoremap <space> za 
"----------Stop python PEP 8 stuff--------------

"js stuff"
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2
"autocomplete
let g:ycm_autoclose_preview_window_after_completion=1

"custom keys
let mapleader="<F4>"
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>
"
call togglebg#map("<F5>")
"colorscheme zenburn
"set guifont=Monaco:h14

let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree

Plug 'pangloss/vim-javascript', { 'for': ['js', 'ts']}
Plug 'pearofducks/ansible-vim'
Plug 'kien/ctrlp.vim'
Plug 'klen/rope-vim'
" Plugin 'davidhalter/jedi-vim'
Plug 'ervandew/supertab'
" Code folding
Plug 'tmhedberg/SimpylFold'
Plug 'cedarbaum/fugitive-azure-devops.vim', {'for': ['azure-pipelines.yml','azure-pipelines.yaml']}

" NERDTree install and setup
Plug 'preservim/nerdtree' 
Plug 'jistr/vim-nerdtree-tabs'
let NERDTreeIgnore=['\.pyc$', '\~$', '.gitignore', '\.so$', '\.rpm'] "ignore files in NERDTree
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>

Plug 'ryanoasis/vim-devicons'
Plug 'adelarsq/vim-matchit'
Plug 'frazrepo/vim-rainbow'
Plug 'junegunn/fzf'
Plug 'itchyny/lightline.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'dense-analysis/ale'
Plug 'tpope/vim-fugitive'
Plug 'preservim/tagbar'
"Plug 'powerline/powerline'
Plug 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}
Plug 'vim-scripts/taglist.vim'
Plug 'vim-scripts/UltiSnips'
Plug 'ardagnir/athame'
Plug 'vim-scripts/ctags.vim'
Plug 'TomasTomecek/sen'
Plug 'vim-vdebug/vdebug', { 'on': 'DebugToggle' }
Plug 'ardagnir/athame'
call plug#end()
set clipboard=unnamed
au BufRead,BufNewFile */playbooks/*.yml set  filetype=yaml.ansible
au BufRead,BufNewFile */playbooks/*.yaml set filetype=yaml.ansible
au BufRead,BufNewFile main.yml  set filetype=yaml.ansible
au BufRead,BufNewFile main.yaml set filetype=yaml.ansible
set statusline+=%"warningmsg"
set statusline+=%{SyntasticStatuslineFlag()}
set the F8 key to enable statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 1

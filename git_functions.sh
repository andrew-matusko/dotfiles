#!/bin/env bash 
set -x
set -v
set -f
set -u
function set-keys(){
    eval `ssh-agent -s`
    KEYS=`ssh-add -l`
    echo "All keys: $KEYS"
    input="input.txt"
    cat $KEYS > input.txt
    while IFS= read -r line
    do
       printf 'Adding key %s from file...\n' "$line" 
       ssh-add ~/.ssh/"$key"
    done < "$input"
}
function git-ac(){ 
    read -p "?Enter a commit comment?" 
    # assume you are in project root  
    # add any tokens
    git-pull
    git-add 
    git commit -m "$1"
    git push    
}
function git-add(){
    git-pull # function
    git add --all
    git add .
}
function git-pull(){
    #$1 ia the name of your working
    # branch, if you have one. Enter it when you call    # the funcrion 
    # e.g. git-pull feature23
    git pull origin master
    git pull origin main
    if [[ -n $1 ]]; then
        git pull origin $1
    fi
}
function create-git-branch(){
    echo "You must be in the root sirectory of your git project."
    # Does it exist? How to check?
    git checkout -b $1
    if [[ $? == 0 ]]; then
        echo "The branch $? has been created sucessfully."
    else
	echo "Branch creation for $?unsuccessful." 
     fi
}
function add-my-keys(){
	if [ ! $(echo $added_keys | grep -o -e id_rss) ]; then
	    ssh-add "$HOME/.ssh/id_rsa"
	fi
}

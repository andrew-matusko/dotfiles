#!/bin/env bash -xveu
yum update -y 
yum install zsh vim vim-wnhanced gcc gcc++ make python3 python3-pip buildahdocker-ce docker-ce-cli docker-ce-rootless-extras tmux xterm-256 git redis-server yamllint ctags sed awk mlocate -y

yum install htop ntop net-tools zsh vim vim-enhanced vim-filesystem git -y
yum install docker-logrotate podman buildah skopeo cadvisor kubernetes-cni -y
yum install cri-tools flannel containernetworking-plugin golang -y
yum install podman-docker docker-distribution -y
yum install neovim -y
systemctl enable --now  docker
systemctl start docker 

#Temporarily shut off swap. There can be no swap on a kubernetes machine
echo "Temporarily shut off swap. There can be no swap on a kubernetes machine"
echo "Shutting off swap now."

swapoff -a 

cd $HOME
echo "Current working directory is: ~/.dotfiles"
echo "\$HOME == $HOME. Is this correct?"
if [[ -d $HOME/dotfiles ]]; then
    rm -rf $HOME/dotfiles
    git clone https://gitlab.com/andrew-matusko/dotfiles.git ~/.dotfiles
elif [[ -d $HOME/.dotfiles ]]; then
    rm -rf ~/.dotfiles
    git clone https://gitlab.com/andrew-matusko/dotfiles.git ~/.dotfiles
fi

DOTFILES_DIR=$HOME/.dotfiles
DOTFILES_DIR=$HOME/dotfiles

if [[ ! -d $DOTFILES_DIR ]]; then
	rm -f $HOME/.profile
	rm -f $HOME/.bashrc
	rm -f $HOME/.bash_profile
	rm -f $HOME/.vimrc
	rm -f $HOME/.zshrc
	ln -s ~/.dotfiles/.profile $HOME/.profile
	ln -s ~/.dotfiles/.bashrc  $HOME/.bashrc
	ln -s ~/.dotfiles/.bash_profile $HOME/.bash_profile
	ln -s ~/.dotfiles/.vimrc   $HOME/.vimrc
	ln -s ~/.dotfiles/.zshrc   $HOME/.zshrc

if [[  -f  $HOME/.tmux.conf ]]; then
    unlink ~/.tmux.conf
fi
if [[ -f $HOME/.tmux.conf.local ]]; then
    unlink $HOME/.tmux.conf.local
fi
if [[ -d $HOME/.tmux ]]; then
    rm -rf $HOME/.tmux
fi

git clone https://github.com/gpakosz/.tmux.git $HOME/dotfiles/.

ln -s ~/.dotfiles/.tmux   $HOME/.tmux
ln -s ~/.dotfiles/.tmux/.tmux.conf $HOME/.tmux.conf
ln -s ~/.dotfiles/.tmux/.tmux.conf.local $HOME/.tmux.conf.local
chown -R $(USER):$(USER) $HOME/.
ln -s $PWD/.tmux   $HOME/.tmux
ln -s $PWD/.tmux/.tmux.conf $HOME/.tmux.conf
ln -s $PWD/.tmux/.tmux.conf.local $HOME/.tmux.conf.local
chown -R $USER_NAME  $HOME/.

chmod -R 755 $HOME
if [ -d $HOME/.tmux ]; then
  if [ -f $HOME/dotfiles/.tmux.conf ]; then
    ln -s $HOME/dotfiles/.tmux.conf $HOME/.tmux/.
  else
    yum install tmux
    ln -s $HOME/dotfiles/.tmux.conf $HOME/tmux/.
  fi  
fi
# Enable the right repos
subscription-manager repos --enable=rhel-7-server-extras-rpms
cd /opt
yum install wget git zsh net-tools tmux vim vim-enhanced neovim -y
mkdir -p /go/installer
mkdir -p /go/data
mkdir -p /go/projects
mkdir -p /go/examples
mkdir -p /go/golang
mkdir -p /go/home
wget https://golang.org/dl/go1.17.3.linux-amd64.tar.gz /go/installer/.
tar -xvzf go.1.17.3.linux-amd64.tar.gz /go/golang/.
groupadd gogroup
adduser gouser -d /go/home -g gogroup -G docker -pintelsat123!
chown -R gouser:gogroup /go
chmod -R 755 /go
export GOPATH=/go/golang
go get github.com/tools/godep
gpg2 --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
curl -sSL https://get.rvm.io | bash -s stable
rvm install 3.0.2 && gem install tmuxinator
#export GOPATH=/opt/godep go get github.com/tools/godep
#ln -s /opt/godep/bin/godep /usr/local/bin/
#exit $?
export GOPATH=/opt/godep go get github.com/tools/godep
ln -s /opt/godep/bin/godep /usr/local/bin/
exit $?

fi                                                                                                                                             │
if [[ -d ~/.tmux ]]; then                                                                                                                      │
        rm -rf ~/.tmux                                                                                                                         │
        cp -rf ~/dotfiles/.tmux ~/.                                                                                                            │
        ln -s ~/dotfiles/.tmux.conf ~/.tmux.conf                                                                                               │
        ln -s  ~/dotfiles/.tmux.conf.local ~/.tmux.conf.local                                                                                  │
        source ~/.tmux.conf                                                                                                                    │
        source ~/.tmux.conf.local                                                                                                              │
fi                                                                                                                                             │
                                                                                                                                               │
function dotfiles(){                                                                                                                           │
        # @param $1 is the name of the file with no directory.                                                                                 │
        # It is assumed that dotfiles dir is in the home dir.                                                                                  │
        if [[ -f $1 ]]; then                                                                                                                   │
                rm -rf ~/$1                                                                                                                    │
                unlink ~/$1                                                                                                                    │
                ln -s ~/dotfiles/$1 ~/$1                                                                                                       │
                source ~/$1                                                                                                                    │
        elseif [[ -d $1 ]]; then                                                                                                               │
                rm -rf ~/$1                                                                                                                    │
                cp -rf ~/dotfiles/$1 ~/$1                                                                                                      │
        fi                                                                                                                                     │
        if [[ $1 == '.vimrc' ]]; then                                                                                                          │
                        vim +PluginInstall +qall                                                                                               │
        fi                                                                                                                                     │
dotfiles .vimrc 
function create-dir(){
    if [[ -d "$HOME/$1" ]]; then
        rm -rf $1
        cp -rf ~/dotfiles/$1 ~/
    fi
}
create-dir "~/.vim"

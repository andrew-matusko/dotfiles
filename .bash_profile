# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

export PATH=$PATH:$HOME/.local/bin:$HOME/bin
export PATH="$HOME/.brew/bin:$HOME/.brew/sbin:$PATH"
export PATH=/Users/andrew.matusko/Library/Python/3.8/bin:$HOME/homebrew/bin:$PATH

export KUBECONFIG=/docker/rke/kube_config_cluster.yml

# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
if [ -f /etc/zshrc ]; then
  source /etc/zshrc
fi
if [ -f /etc/profile ]; then
  source /etc/profile
fi
source ~/.profile
source ~/.bash_profile
source ~/.tmux.conf
source ~/.tmux.conf.local
#source ~/.zshrc
eval `ssh-agent -s` &> /dev/null
$(ssh-add ~/.ssh/id_rsa) 2>&1 > /dev/null
$(ssh-add ~/.ssh/rancher) 2>&1 > /dev/null 
$(ssh-add ~/.ssh/id_dsa) 2>&1 > /dev/null 

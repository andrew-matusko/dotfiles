#!/bin/bash -xve

yum install htop ntop net-tools zsh vim vim-enhanced vim-filesystem git -y
yum install docker-logrotate podman buildah skopeo cadvisor kubernetes-cni -y
yum install cri-tools flannel containernetworking-plugin golang -y
yum install podman-docker docker-distribution -y

systemctl enable --now  docker
systemctl start docker

#Temporarily shut off swap. There can be no swap on a kubernetes machine
echo "Temporarily shut off swap. There can be no swap on a kubernetes machine"
echo "Shutting off swap now."

swapoff -s


cd $HOME
echo "Current working directory is: $PWD"
echo "\$HOME == $HOME. Is this correct?"
rm -f $HOME/.profile
rm -f $HOME/.bashrc
rm -f $HOME/.bash_profile
rm -f $HOME/.vimrc
rm -f $HOME/.zshrc
ln -s $HOME/dotfiles/.profile $HOME/.profile
ln -s $HOME/dotfiles/.bashrc  $HOME/.bashrc
ln -s $HOME/dotfiles/.bash_profile $HOME/.bash_profile
ln -s $HOME/dotfiles/.vimrc   $HOME/.vimrc
ln -s $HOME/dotfiles/.zshrc   $HOME/.zshrc
#if tmux folder exists, copy .tmux.conf to the tmux folder with a symlink.
#Else, install tmux, then do it

cd $HOME
git clone https://github.com/gpakosz/.tmux.git
ln -s -f .tmux/.tmux.conf
cp .tmux/.tmux.conf.local .


if [ -d $HOME/tmux ]; then
  if [ -f $HOME/dotfiles/.tmux.conf ]; then
    ln -s $HOME/dotfiles/.tmux.conf $HOME/tmux/.
  else
    yum install tmux
    ln -s $HOME/dotfiles/.tmux.conf $HOME/tmux/.
  fi
fi
# Enable the right repos
subscription-manager repos --enable=rhel-7-server-extras-rpms
cd /opt
GOPATH=/opt/godep go get github.com/tools/godep
ln -s /opt/godep/bin/godep /usr/local/bin/
